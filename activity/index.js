const students = [];

function addStudent(name) {
	students.push(name);
	console.log(name + " has been added to the list")
}

function countStudents() {
	console.log("the total students are " + students.length);
}

function printStudents() {
	console.log(students);
	students.sort();
	console.log(students);

	students.forEach(function(name) {
		console.log(name);
	})
}

addStudent("carlo");
addStudent("aarlo");
addStudent("zarlo");
addStudent("xarlo");
addStudent("garlo");

console.log(students);
countStudents();
printStudents();

// stretch goal

function findStudent(arr, query) {
	let filter = arr.filter(function(a) {
		return a.toLowerCase().indexOf(query.toLowerCase()) !== -1
	})

	

	if (filter.length == 1) {
		console.log(filter[0] + " is an enrollee (single item)")
	} else if (filter.length > 1) {
		filter.forEach(function(a) {
			console.log(a + " are enrollees")
		})
	} else if (filter.length == 0) {
		console.log(query + " is not an enrollee")
	}
}


findStudent(students, "xyz");
findStudent(students, "ca");
findStudent(students, "arl");
let stud1 = "2020-01-17";
let stud2 = "2020-03-15";
let stud3 = "2020-04-14";
let stud4 = "2020-02-12";

let students = [];

students = students.concat(stud1, stud2, stud3, stud4);


console.log(students);

let grades = [75, 85.5, 92, 94];

let computerBrands = ["Acer", "Asus", "Lenovo", "Apple", "Redfox", "Gateway"]
console.log(computerBrands);

let tasks = [
	'drink html',
	'eat js',
	'inhale css',
	"bake sass"
]

console.log(tasks);

console.log(students[3]);


const fruits = ["Apple", "Orange", "Kiwi", "Dragonfruit"];
console.log("value: ", fruits);

fruits.push("Mango");
console.log("value: ", fruits);

let pop = fruits.pop();
console.log("value: ", fruits, pop);

fruits.unshift(pop, pop);
console.log("value: ", fruits);

let shift = fruits.shift();
console.log("value: ", fruits, shift);

fruits.sort();
console.log("value: ", fruits);

fruits.reverse();
console.log("value: ", fruits);

fruits.unshift("Apple");
console.log("value: ", fruits);

let appleIndex = fruits.indexOf("Apple");
console.log(appleIndex);

let lastApple = fruits.lastIndexOf("Apple");
console.log(lastApple);


console.log("value: ", fruits.toString());

const fruits2 = ["Grapes", "Lemon", "Pears", "Plum"];

let fruits3 = fruits.concat(fruits2);
console.log(fruits3);

console.log(fruits.join(' | '));

const users = ['blue', 'alexis', "bianca", "nikko", "adrian"];

users.forEach(function(a) {

	if (a == "blue") {
		console.log('Name of Student: ', a)

	}

});

const numList = [1, 2, 3, 4, 5, 6, 7, 8, 9];
const evenNum = [];
numList.forEach(function(a) {
	if (a % 2 ==0) {
		console.log(a);
		evenNum.push(a);
	}
})

console.log(evenNum);

const numData = numList.map(function(a) {
	return a * a;
})

console.log(numList);
console.log(numData);

const chessBoard = [
	["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
	["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
	["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
	["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
	["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
	["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
	["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
	["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
]

console.log(chessBoard[0][1]);